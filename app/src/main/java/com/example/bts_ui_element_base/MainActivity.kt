package com.example.bts_ui_element_base

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private var TAG: String = "MainActivity"
    private lateinit var button: Button
    private lateinit var button3: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.button)
        button.setOnClickListener { view ->
            Toast.makeText(this, "You have pressed the button!!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "You have pressed the button, here is the log!!")
        }

        button3 = findViewById(R.id.button3)
        button3.setOnClickListener { view ->
            //     Toast.makeText(this, "You have pressed the button!!!",   Toast.LENGTH_SHORT).show()
            Log.e(TAG, "Dont Press My Buttons!!")
        }
    }
}